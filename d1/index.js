// fetch() method in JavaScript is used to send request in the server and load the received response  in the webpage. The request and response is in JSON format.

/*
    Syntax: 
    fetch('url', option)
        // url - this is the url which the request is to be made (endpoint).
        // options - array of properties that contains the HTTP method, body of the request, and headers.

*/

// GET post data
fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
// invoke the showPosts()
.then((data) => showPosts(data));

// Add post data.
// this will trigger an event that will add a new posst in our mock database upon clicking the "Create" button.

                        // selects a form
document.querySelector("#form-add-post").addEventListener("submit", (e) => {

    // Prevents the page from loading
    e.preventDefault();
    fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: JSON.stringify({
            title:  document.querySelector("#txt-title").value,
            body: document.querySelector("#text-body").value,
            userId: 1,
        }),
        headers: {
            "Content-Type": "application/json"
        }
    })
    .then((response) => response.json())
    .then((data) =>{
        console.log(data);
        alert("Successfully Added!");
    })

    // resets the state of our input into blanks after submitting new posts
    document.querySelector("#txt-title").value = null;
    document.querySelector("#text-body").value = null;
});

// -----------------------------
// View Posts
const showPosts = (posts) => {
    let postEntries = "";

    // We will use forEach() to display each movie inside oour mock database
    posts.forEach((post) => {
        postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
    });

    // To check what is stored in the postEntries variables
    // console.log(postEntries);

    // To replace the content of the "div-post-entries"
    document.querySelector("#div-post-entries").innerHTML = postEntries;
};

// --------------------------------
// Edit Post button

const editPost = (id) => {
    // Contain the value of the title and body in a variable
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    // Pass the id, title, and body of the movie post to be updated in the Edit Post/Form.
    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#text-edit-body").value = body;

    // To remove the disabled property
    document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

//Update post
// This will trigger an event that will update a post upon clicking the Update button

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
    e.preventDefault();
    let id = document.querySelector("#txt-edit-id").value;

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        method: "PUT",
        body: JSON.stringify({
            id: id,
            title: document.querySelector("#txt-edit-title").value,
            body: document.querySelector("#text-edit-body").value,
            userId: 1
        }),
        headers: {
            "Content-Type": "application/json"
        }
    })
    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        alert("Successfully updated!");
        document.querySelector("#txt-edit-title").value = null;
        document.querySelector("#text-edit-body").value = null;

        // Add a attribute in a HTML element
        document.querySelector("#btn-submit-update").setAttribute("disabled", true);

    })
});

// Delete post
const deletePost = (id) => {

    for(i = 0; i < posts.length; i++){
        
        if(posts[i].id == id){
            posts.splice(i, 1);
        
        showPosts();

        break;
        }
    }
};

// Other approach (for delete)
/*

const deletePost = (id) => {
    // filter method will save the posts that are not equal to the id parameter
    posts = posts.filter((post) => {
        if(post.id !== id){
            return post;
        }

        // .remove()method removes an element (or node) from the document.

        document.querySelector(`#post-${id}`).remove();
    })
}

*/

